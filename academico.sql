-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2021 at 06:40 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academico`
--

-- --------------------------------------------------------

--
-- Table structure for table `alunos`
--

CREATE TABLE `alunos` (
  `id` int(11) NOT NULL,
  `matricula` varchar(7) COLLATE latin1_general_ci NOT NULL,
  `cpf` varchar(11) COLLATE latin1_general_ci NOT NULL,
  `nome` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `gitlab` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `celular` varchar(11) COLLATE latin1_general_ci NOT NULL,
  `turma` enum('AUT2D1','AUT2D2','AUT2D3','') COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `alunos`
--

INSERT INTO `alunos` (`id`, `matricula`, `cpf`, `nome`, `email`, `gitlab`, `celular`, `turma`) VALUES
(1, '1234567', '12345678900', 'Lula Bolsonaro da Silva', 'lula.bolsonaro@gmail.com', 'lula.bolsonaro', '31917132022', 'AUT2D3');
INSERT INTO `alunos` (`id`, `matricula`, `cpf`, `nome`, `email`, `gitlab`, `celular`, `turma`) VALUES (NULL, '234567', '11111111111', 'Joaquim dos Santos', 'joaquim@email.com', 'joaq.santos', '123456789', 'AUT2D2');
--
-- Indexes for dumped tables
--

--
-- Indexes for table `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alunos`
--
ALTER TABLE `alunos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
